<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
<html>
	<head>
			<title>Moja kolekcja gier</title>
			<link rel="stylesheet" href="style.css" type="text/css" />
			</head>
	<body>
	<div class="lista">
	<h1>Moja kolekcja gier</h1>
		<div class="nav">
		<div class="id">nr.</div>
		<div class="title">tytuł</div>
		<div class="genre">gatunek</div>		
		<div class="prod">producent</div>
		<div class="year">rok wydania</div>
		</div>	
									<xsl:apply-templates select="kolekcja/gry/gra"/>
	</div>
	</body>
</html>
</xsl:template>
<xsl:template match="gra">

		<div class="gra">
			<div class="id">
				#<xsl:value-of select="@nr"/>
			</div>
			<div class="title">
				<xsl:value-of select="tytul"/>
			</div>
			<div class="genre">
				<xsl:value-of select="gatunek"/>
			</div>			
			<div class="prod">
				<xsl:value-of select="producent"/>
			</div>
			<div class="year">
		<xsl:value-of select="rok_wydania"/>
			</div>
		</div>
</xsl:template>

</xsl:stylesheet>
	
