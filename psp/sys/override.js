/*-------------------------------------------------------------------------*/
// GENERAL INSERT FUNCTION
// opentext : opening tag
// closetext: closing tag, used if we have selected text
/*-------------------------------------------------------------------------*/

function wrap_tags(tag,areaid)
{
	if (tag != "center") {
		opentext = '<'+tag+'>';
		closetext = '</'+tag+'>';
	} else {
		opentext = '<p style="text-align:center;">';
		closetext = '</p>';
	}
	
	
	postfieldobj = document.getElementById(areaid);
	
	var has_closed = false;
	
	var ss = postfieldobj.selectionStart;
	var st = postfieldobj.scrollTop;
	var es = postfieldobj.selectionEnd;
	
	if (es <= 0)
	{
		es = postfieldobj.textLength;
	}
	
	var start  = (postfieldobj.value).substring(0, ss);
	var middle = (postfieldobj.value).substring(ss, es);
	var end    = (postfieldobj.value).substring(es, postfieldobj.textLength);
	
	//-----------------------------------
	// text range?
	//-----------------------------------
	
	if ( postfieldobj.selectionEnd - postfieldobj.selectionStart > 0 )
	{
		middle = opentext + middle + closetext;
	}
	
	postfieldobj.value = start + middle + end;
	
	var cpos = ss + (middle.length);
	
	postfieldobj.selectionStart = cpos;
	postfieldobj.selectionEnd   = cpos;
	postfieldobj.scrollTop      = st;

	
	postfieldobj.focus();

	return has_closed;
}


